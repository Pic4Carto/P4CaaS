/*
 * This file is part of P4CaaS.
 * 
 * P4CaaS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * P4CaaS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with P4CaaS.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function(app) {
	const ctrl = require('../controllers/P4CaaSController');
	
	app.route('/')
		.get(ctrl.default);
	
	app.route('/search')
		.get(ctrl.default);
	
	app.route('/search/around')
		.get(ctrl.searchAround);
	
	app.route('/search/bbox')
		.get(ctrl.searchBbox);
	
	app.route('/fetchers')
		.get(ctrl.fetchers);
	
	app.route('/summary')
		.get(ctrl.default);
	
	app.route('/summary/bbox')
		.get(ctrl.summaryBbox);
};
